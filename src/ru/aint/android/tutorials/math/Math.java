package ru.aint.android.tutorials.math;

/**
 * Additional math functions.
 */
public abstract class Math {
    
    /**
     * Computes logarithm of <code>x</code> of base of <code>base</code>.
     * 
     * @param x Argument of logarithm.
     * @param base Base of logarithm.
     * 
     * @return Logarithm of <code>x</code> of base of <code>base</code>.
     */
    public static float log(float x, float base) {
        float result = (float)(java.lang.Math.log(x) / java.lang.Math.log(base));
        return result;
    }

    /**
     * Computes logarithm of <code>x</code> of base of <code>base</code>.
     * 
     * @param x Argument of logarithm.
     * @param base Base of logarithm.
     * 
     * @return Logarithm of <code>x</code> of base of <code>base</code>.
     */
    public static double log(double x, double base) {
        return (java.lang.Math.log(x) / java.lang.Math.log(base));
    }
}
