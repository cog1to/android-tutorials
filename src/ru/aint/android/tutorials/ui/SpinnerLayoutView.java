package ru.aint.android.tutorials.ui;

import java.util.ArrayList;

import ru.aint.android.tutorials.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Layout view emulating spinner wheel.
 */
public class SpinnerLayoutView extends RelativeLayout {
    
    /**
     * Spinner listener interface.
     */
    public interface SpinnerLayoutViewListener {
        /**
         * Called when spinner layout has set his value to a new one.
         */
        public void onValueSelected(float value);
    }
    
    /**
     * Data adapter interface.
     */
    public interface SpinnerLayoutViewAdapter {
        /**
         * Should return content of an item at {@linkplain index}.
         * 
         * @param index Index of an item.
         * 
         * @return Content string representing the item at given {@linkplain index}.
         */
        public String getItemAtIndex(int index);
        
        /**
         * Should return overall number of items.
         * 
         * @return Number of items in list.
         */
        public int getNumberOfItems();
    }
    
    /**
     * Default font color.
     */
    private final static int DEFAULT_FONT_COLOR_ID = android.R.color.secondary_text_light; 
    
    /**
     * Test size for content views.
     */
    private final static int DEFAULT_CONTENT_TEXT_SIZE = 40;
    
    /**
     * Spacing between content views.
     */
    private final static int DEFAULT_MIN_CONTENT_SPACING = 0;
    
    /**
     * None spacing mode.
     */
    private final static int CONTENT_SPACING_NONE = 1;
    
    /**
     * Grow spacing mode.
     */
    private final static int CONTENT_SPACING_GROW = 2;
    
    /**
     * Shrink spacing mode.
     */
    private final static int CONTENT_SPACING_SHRINK = 3;
    
    /**
     * Any spacing mode.
     */
    private final static int CONTENT_SPACING_ANY = 4;
    
    /**
     * Number of visible elements.
     */
    private final static int NUMBER_OF_VISIBLE_ELEMENTS = 3;
    
    /**
     * Number of visible elements.
     */
    private final static int NUMBER_OF_ACTIVE_ELEMENTS = 5;
    
    /**
     * Speed value at which scrolling should stop.
     */
    private final static float SCROLL_SPEED_STOP_THRESHOLD = 1.0f;
    
    /**
     * Speed value at which scrolling should start
     */
    private final static float SCROLL_SPEED_DETECT_THRESHOLD = 1.0f; 
    
    /**
     * Max allowed scroll speed.
     */
    private final static float SCROLL_SPEED_MAX = 10.0f;
    
    /**
     * Length of the touch move event at which we should detect scroll.
     */
    private final static float SCROLL_LENGTH_DETECT_THRESHOLD = 0.2f; 
    
    
    
    /**
     * List of content views.
     */
    private ArrayList<MovingTextView> mContentViews;
    
    /**
     * Content spacing.
     */
    private int mContentSpacing = DEFAULT_MIN_CONTENT_SPACING;
    
    /**
     * Font size.
     */
    private int mFontSize = DEFAULT_CONTENT_TEXT_SIZE;
    
    /**
     * Minimum spacing between elements.
     */
    private int mStretchMode = CONTENT_SPACING_SHRINK;
    
    /**
     * Font color.
     */
    private int mFontColor = getResources().getColor(DEFAULT_FONT_COLOR_ID);
    
    /**
     * Should stay in bounds indicator.
     */
    private boolean mShouldStayInBounds = true;
    
    /**
     * Current progress value.
     */
    private volatile float mCurrentIndex = 0;
    
    /**
     * Current value index.
     */
    private int mSelectedIndex;
    
    /**
     * Value change listener.
     */
    private SpinnerLayoutViewListener mListener;
    
    /**
     * Previous touch event location.
     */
    private float mPreviousTouch = Float.NaN;
    
    /**
     * Start touch event location.
     */
    private volatile float mStartTouch = Float.NaN;
    
    /**
     * Touch start timestamp.
     */
    private long mStartTouchTime = Long.MIN_VALUE; 
    
    /**
     * Current swipe direction.
     */
    private int mCurrentSwipeDirection = 0;
    
    /**
     * Content width.
     */
    private float mContentWidth = 0.0f;
    
    /**
     * Max value.
     */
    private int mMaxIndex;
    
    /**
     * Data adapter for spinner layout view.
     */
    private SpinnerLayoutViewAdapter mAdapter;
    
    /**
     * Current animation thread.
     */
    private Thread mCurrentAnimationThread;
        
    /**
     * Constructor to create view from code.
     * 
     * @param context Application context.
     */
    public SpinnerLayoutView(Context context) {
        super(context);
        setup(null);
    }
    
    /**
     * Constructor to inflate view from XML markup.
     * 
     * @param context Application context.
     * @param attrs View attributes.
     */
    public SpinnerLayoutView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup(attrs);
    }
    
    /**
     * Constructor to inflate view from XML markup with specified style.
     * 
     * @param context Application context.
     * @param attrs View attributes.
     * @param defStyle Default style ID.
     */
    public SpinnerLayoutView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup(attrs);
    }
    
    /**
     * Initial setup.
     * 
     * @param attrs View attributes.
     */
    private void setup(AttributeSet attrs) {
        mMaxIndex = 0;
        mSelectedIndex = mMaxIndex / 2;
        mCurrentIndex = mSelectedIndex;
        
        parseAttributes(attrs);
        createLayout(NUMBER_OF_ACTIVE_ELEMENTS);
    }
    
    /**
     * Parses attributes list.
     * 
     * @param attrs Attributes list.
     */
    private void parseAttributes(AttributeSet attrs) {
        TypedArray styleAttrs = getContext().obtainStyledAttributes(attrs, R.styleable.SpinnerLayoutView);
        
        // Cycle through all provided attributes.
        for (int i = 0; i < styleAttrs.getIndexCount(); i++) {
            int attr = styleAttrs.getIndex(i);
            switch (attr) {
                case R.styleable.SpinnerLayoutView_fontSize:
                    // User has provided font name, try to load it.
                    try {
                        int fontSize = styleAttrs.getDimensionPixelSize(i, DEFAULT_CONTENT_TEXT_SIZE);
                        if (fontSize > 0) {
                            mFontSize = fontSize;                            
                        }
                    } catch (Exception ex) {
                        Log.d("SpinnerLayoutView", "Error while reading fontSize: " + ex.getMessage());
                    }
                    break;
                case R.styleable.SpinnerLayoutView_stretchMode:
                    try {
                        int stretchMode = styleAttrs.getInteger(i, CONTENT_SPACING_SHRINK);
                        if (stretchMode > 0) {
                            mStretchMode = stretchMode;
                        }
                    } catch (Exception ex) {
                        Log.d("SpinnerLayoutView", "Error while reading stretchMode: " + ex.getMessage());
                    }
                    break;
                case R.styleable.SpinnerLayoutView_fontColor:
                    // User has provided font name, try to load it.
                    try {
                        int fontColor = styleAttrs.getColor(i, getResources().getColor(DEFAULT_FONT_COLOR_ID));
                        mFontColor = fontColor;                        
                    } catch (Exception ex) {
                        Log.d("SpinnerLayoutView", "Error while reading fontSize: " + ex.getMessage());
                    }
                    break;
                default:
                    break;
            }
        }
        
        
        
        styleAttrs.recycle();
    }
    
    /**
     * Creates layout for the given number of elements.
     * 
     * @param count Number of elements to create.
     */
    private void createLayout(int count) {
        this.removeAllViews();
        mContentViews = new ArrayList<SpinnerLayoutView.MovingTextView>();
        
        for (int idx = 0; idx < count; idx++) {
            // Create text view.
            MovingTextView contentView = new MovingTextView(getContext());
            contentView.setTextSize(mFontSize);
            contentView.setTextColor(mFontColor);
            contentView.setGravity(Gravity.CENTER);
            
            contentView.setText(String.format("%02d", idx));
            
            // Define the layout parameters of the TextView.
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);        
            lp.addRule(RelativeLayout.CENTER_IN_PARENT);
            contentView.setLayoutParams(lp);
            
            mContentViews.add(contentView);
            this.addView(contentView);
        }
        
        // Set the initial content for views.
        this.updateSubviews();
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float currentTouch = event.getX();
        
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // Stop any scrolling animation in progress.
                this.stop();
                
                mStartTouchTime = System.currentTimeMillis();
                mStartTouch = currentTouch;
                mPreviousTouch = currentTouch;
                
                break;
            case MotionEvent.ACTION_UP:                
                long currentTime = System.currentTimeMillis();
                float touchDuration = (float)(currentTime - mStartTouchTime) / 1000.0f;
                float touchLength = Math.abs((currentTouch - mStartTouch) / mContentWidth);
                float touchSpeed = Math.abs(touchLength / touchDuration);
                
                mPreviousTouch = Float.NaN;
                mStartTouch = Float.NaN;
                mStartTouchTime = Long.MIN_VALUE;
                
                if ((touchLength >= SCROLL_LENGTH_DETECT_THRESHOLD) && (touchSpeed >= SCROLL_SPEED_DETECT_THRESHOLD)) {
                    // If swipe is detected, perform swipe animation.
                    this.swipe(mCurrentSwipeDirection, Math.min(touchSpeed, SCROLL_SPEED_MAX));
                } else {
                    // Otherwise trim to nearest value.
                    this.trim();
                }
                
                break;
            case MotionEvent.ACTION_MOVE:
                if (mPreviousTouch == Float.NaN) {
                    return false;
                }
                
                // Determine swipe direction.
                int swipeDirection = 0;
                if (currentTouch < mPreviousTouch) {
                    swipeDirection = -1;
                } else if (currentTouch > mPreviousTouch) {
                    swipeDirection = 1;
                }
                
                // If swipe direction changed, remember the starting point of new swipe.
                if ((swipeDirection != 0) && (swipeDirection != mCurrentSwipeDirection)) {
                    mStartTouchTime = System.currentTimeMillis();
                    mStartTouch = mPreviousTouch;
                }
                mCurrentSwipeDirection = swipeDirection;
                
                this.updateProgress((currentTouch - mPreviousTouch) / mContentWidth);                
                mPreviousTouch = currentTouch;
        }
        
        return true;                
    }
    
    @SuppressLint("DrawAllocation")
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {        
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        
        mContentSpacing = 5;
        
        // Measure all childs.
        this.updateSubviews();
        for (MovingTextView textView : mContentViews) {
            textView.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
        }
        int width = mContentViews.get(0).getMeasuredWidth();
        int height = mContentViews.get(0).getMeasuredHeight();
        Point measuredTextSize = new Point(width, height);
        
        int measuredWidth = (measuredTextSize.x*NUMBER_OF_VISIBLE_ELEMENTS) + (mContentSpacing*(NUMBER_OF_VISIBLE_ELEMENTS-1));
        
        int maxAllowedWidth = MeasureSpec.getSize(widthMeasureSpec);
        boolean shouldOccupyWidth = false;
        
        int maxAllowedHeight = MeasureSpec.getSize(heightMeasureSpec);
        boolean shouldOccupyHeight = false;
        
        // Process provided layout params.
        LayoutParams layout = (LayoutParams)this.getLayoutParams();
        
        if (layout.width == LayoutParams.WRAP_CONTENT) {
            if (!mShouldStayInBounds) {
                maxAllowedWidth = measuredWidth;
            }
        } else if (layout.width != LayoutParams.MATCH_PARENT) {
            maxAllowedWidth = layout.width;
            shouldOccupyWidth = true;
        } else {
            shouldOccupyWidth = true;
        }
        
        if (layout.height == LayoutParams.WRAP_CONTENT) {
            if (!mShouldStayInBounds) {
                maxAllowedHeight = measuredTextSize.y;
            }
        } else if (layout.height != LayoutParams.MATCH_PARENT) {
            maxAllowedHeight = layout.height;
            shouldOccupyHeight = true;
        } else {
            shouldOccupyHeight = false;
        }
        
        if (mStretchMode != CONTENT_SPACING_NONE) {
            if ((measuredWidth < maxAllowedWidth) && shouldOccupyWidth && ((mStretchMode == CONTENT_SPACING_GROW) || (mStretchMode == CONTENT_SPACING_ANY))) {
                // try to adjust content spacing if we have any space left unused.
                int maxSpacing = mContentSpacing;
                int newMeasuredWidth = (measuredTextSize.x*NUMBER_OF_VISIBLE_ELEMENTS) + ((maxSpacing+1)*(NUMBER_OF_VISIBLE_ELEMENTS-1));
                while (newMeasuredWidth < maxAllowedWidth) {
                    maxSpacing += 1;
                    newMeasuredWidth = (measuredTextSize.x*NUMBER_OF_VISIBLE_ELEMENTS) + ((maxSpacing+1)*(NUMBER_OF_VISIBLE_ELEMENTS-1));
                }
                mContentSpacing = maxSpacing;                
            } else if ((measuredWidth > maxAllowedWidth) && ((mStretchMode == CONTENT_SPACING_SHRINK) || (mStretchMode == CONTENT_SPACING_ANY))) {
                // try to decrease content spacing if standard spacing is to wide.
                int minSpacing = mContentSpacing;
                int newMeasuredWidth = (measuredTextSize.x*NUMBER_OF_VISIBLE_ELEMENTS) + ((minSpacing-1)*(NUMBER_OF_VISIBLE_ELEMENTS-1));
                while ((newMeasuredWidth > maxAllowedWidth) && (Math.abs(minSpacing) < measuredTextSize.x)) {
                    minSpacing -= 1;
                    newMeasuredWidth = (measuredTextSize.x*NUMBER_OF_VISIBLE_ELEMENTS) + ((minSpacing-1)*(NUMBER_OF_VISIBLE_ELEMENTS-1));
                }
                mContentSpacing = minSpacing;
            }
        }        
        
        // Calculate content size.
        mContentWidth = (measuredTextSize.x*NUMBER_OF_VISIBLE_ELEMENTS) + (mContentSpacing*(NUMBER_OF_VISIBLE_ELEMENTS-1));
        
        // Get actual size.
        int actualWidth = (shouldOccupyWidth || (mShouldStayInBounds && (measuredWidth > maxAllowedWidth))) ? maxAllowedWidth : measuredWidth;
        int actualHeight = shouldOccupyHeight ? maxAllowedHeight : measuredTextSize.y;
        
        // Measure all childs.
        this.updateSubviews();
        for (MovingTextView textView : mContentViews) {
            textView.measure(MeasureSpec.makeMeasureSpec(actualWidth, MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(actualHeight, MeasureSpec.AT_MOST));
        }
        
        // Measure self.
        super.onMeasure(MeasureSpec.makeMeasureSpec(actualWidth, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(actualHeight, MeasureSpec.EXACTLY));        
        this.layoutSubviews();
    }
    
    /**
     * Performs subviews layout.
     */
    private void layoutSubviews() {        
        // Calculate overall content size & size for a single value step.        
        float stepProgress = mContentWidth / NUMBER_OF_VISIBLE_ELEMENTS;
        
        int middleIndex = mContentViews.size() / 2;
        for (int idx = 0; idx < mContentViews.size(); idx++) {
            MovingTextView item = mContentViews.get(idx);
            
            int itemIndex = 0;
            // Views to the left & right of middle view occupy the same "cell".
            if (idx < middleIndex) {
                itemIndex = Math.max((idx - middleIndex), -1);
            } else if (idx > middleIndex) {
                itemIndex = Math.min((idx - middleIndex), 1);
            }
            
            // Relative shift for the view according to the current progress value.
            float itemProgress = (float)(idx - middleIndex) + (mSelectedIndex - mCurrentIndex);
            
            Point itemPosition = null;
            // Views to the left & right of middle view occupy the same "cell".
            if (itemIndex < 0) {
                itemPosition = new Point((int)Math.floor(Math.max(itemIndex, itemProgress)*stepProgress), 0);
            } else if (itemIndex > 0) {
                itemPosition = new Point((int)Math.floor(Math.min(itemIndex, itemProgress)*stepProgress), 0);
            } else {
                itemPosition = new Point((int)Math.floor(itemProgress*stepProgress), 0);
            }
            
            synchronized (item) {
                float fadeRatio = 1.0f - (Math.abs(itemProgress) / 2.0f);
                item.setAlpha(fadeRatio);
                item.setPositionAndScale(itemPosition, fadeRatio);
            }
        }
    }
    
    /**
     * Updates the text in content views.
     */
    private void updateSubviews() {
        int middleIndex = mContentViews.size() / 2;
        for (int idx = 0; idx < mContentViews.size(); idx++) {
            MovingTextView item = mContentViews.get(idx);
            item.setTextSize(mFontSize);
            item.setTextColor(mFontColor);
            
            int itemIndex = (idx - middleIndex);
            int itemValueIndex = mSelectedIndex + itemIndex;
            
            // If we're near the value boundaries, side views must be hidden.
            if (((mSelectedIndex + itemIndex) < 0) || ((mSelectedIndex + itemIndex) > mMaxIndex)) {                
                item.setText(null);
            } else {
                if (mAdapter != null) {
                    item.setText(mAdapter.getItemAtIndex(itemValueIndex));
                } else {
                    item.setText(null);
                }
            }
        }
    }
    
    /**
     * Checks the proposed value shift and updates the progress if it is acceptable.
     * 
     * @param diff Difference between the current progress & the proposed progress.
     */
    private void updateProgress(float diff) {
        // If user tries to scroll past the value boundaries, reject that.
        if ((diff > 0) && (((float)mCurrentIndex - diff) < 0)) {
            return;
        } else if ((diff < 0) && (((float)mCurrentIndex - diff) > mMaxIndex)) {
            return;
        }
        
        this.setCurrentIndex(mCurrentIndex - diff);
    }
    
    /**
     * Stops any current animation.
     */
    private void stop() {
        if (mCurrentAnimationThread != null) {
            mCurrentAnimationThread.interrupt();
            mCurrentAnimationThread = null;
        }
    }
    
    /**
     * Performs swipe animation.
     * 
     * @param direction Swipe direction.
     * @param speed Swipe speed.
     */
    private void swipe(int direction, float speed) {
        final Handler h = new Handler();
        mCurrentAnimationThread = new SwipeAnimationThread(h, direction, speed);
        mCurrentAnimationThread.start();
    }
    
    /**
     * Performs trim animation;
     */
    private void trim() {
        final Handler h = new Handler();
        mCurrentAnimationThread = new TrimAnimationThread(h);
        mCurrentAnimationThread.start();
    }
        
    /**
     * Explicitly set the current progress.
     * 
     * @param value New progress value.
     */
    public void setCurrentIndex(float value) {
        if (Math.abs(value - mSelectedIndex) >= 1) {
            mSelectedIndex = Math.round(value);
            mCurrentIndex = value;            
            this.updateSubviews();
            
            if (mListener != null) {
                mListener.onValueSelected(mSelectedIndex);
            }
        } else {
            mCurrentIndex = value;
        }
        
        this.layoutSubviews();
    }   
    
    /**
     * Returns current intermediate index value.
     * 
     * @return Current intermediate index value.
     */
    public float getCurrentIndex() {
        return mCurrentIndex;
    }
    
    /**
     * Sets selected value index.
     * 
     * @param index Index of the selected value.
     * 
     * @throws IllegalArgumentException if index is less than 0 or greater than number of items.
     */
    public void setSelectedIndex(int index) {
        if ((index < 0) || (index > mMaxIndex)) {
            throw new IllegalArgumentException("Index must be between 0 and number of items provided by adapter.");
        }
        
        this.setCurrentIndex(index);
    }
    
    /**
     * Returns index of a currently selected value.
     * 
     * @return Index of a currently selected value.
     */
    public int getSelectedIndex() {
        return mSelectedIndex;
    }
    
    /**
     * Returns current value.
     * 
     * @return Current value.
     */
    public String getCurrentValue() {
        if (mAdapter != null) {
            return mAdapter.getItemAtIndex(mSelectedIndex);
        }
        
        return null;
    }
    
    /**
     * Returns current font size.
     * 
     * @return Current font size.
     */
    public int getFontSize() {
        return mFontSize;
    }
    
    /**
     * Sets font size.
     * 
     * @param size New font size.
     * 
     * @throws IllegalArgumentException in case if {@code size} is less than 0.
     */
    public void setFontSize(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Font size must be greater than 0.");
        }
        
        mFontSize = size;
        this.updateSubviews();
        this.requestLayout();
    }
    
    /**
     * Returns current font color.
     * 
     * @return Current font color value.
     */
    public int getFontColor() {
        return mFontColor;
    }
    
    /**
     * Sets new color value for content views.
     * 
     * @param color New color value to use in content views.
     * 
     * @throws IllegalArgumentException in case of invalid color value.
     */
    public void setFontColor(int color) {
        mFontColor = color;
        this.updateSubviews();
    }
    
    /**
     * Sets listener for value change events.
     * 
     * @param listener View listener.
     */
    public void setListener(SpinnerLayoutViewListener listener) {
        mListener = listener;
    }
    
    /**
     * Checks if view is animating.
     * 
     * @return <b>true</b> if view is animating, <b>false</b> otherwise.
     */
    private boolean isAnimating() {
        return (mCurrentAnimationThread != null);
    }
    
    /**
     * Checks if view is reading touch input.
     * 
     * @return <b>true</b> if view receives touch input, <b>false</b> otherwise.
     */
    private boolean isTouching() {
        return (Float.compare(mStartTouch, Float.NaN) != 0);
    }    
    
    /**
     * Trim animation class.
     */
    public class TrimAnimationThread extends Thread {
        
        /**
         * Trim speed.
         */
        private final float TRIM_SPEED = 2.0f;
        
        /**
         * Sleep interval between updates.
         */
        private final long SLEEP_INTERVAL = 20l; 
        
        /**
         * Handler to access UI thread.
         */
        private Handler mHandler;
        
        /**
         * Main constructor.
         * 
         * @param handler Handler from the UI thread.
         */
        public TrimAnimationThread(Handler handler) {
            mHandler = handler;
        }
        
        @Override
        public void run() {
            try {
                // Get the nearest index.
                int direction = 0;                
                float nearestIndex = Math.round(mCurrentIndex);
                if (nearestIndex < mCurrentIndex) {
                    direction = 1;                    
                } else {
                    direction = -1;
                }
                
                long lastTimestamp = System.currentTimeMillis();
                long timestamp = 0;
                float currentSpeed = TRIM_SPEED;
                float diff = 0;
                
                // Scroll to that nearest index so we don't end up in the middle of the value.
                while (!Thread.interrupted()) {
                    // Get the time diff since last update.
                    timestamp = System.currentTimeMillis();                    
                    diff = (float)(timestamp - lastTimestamp) / 1000.0f;
                    lastTimestamp = timestamp;
                    
                    // Post progress change.
                    final float progressChange = diff*currentSpeed*direction;                    
                    mHandler.post(new Runnable() {
                        public void run() {
                            updateProgress(progressChange);
                        }
                    });
                    
                    // If we've passed nearest value, stop.
                    if ((direction >= 0) && (mCurrentIndex < nearestIndex)) {
                        break;
                    } else if ((direction < 0) && (mCurrentIndex > nearestIndex)) {
                        break;
                    }
                    
                    // We don't need constant updates so we let system rest for some time.
                    sleep(SLEEP_INTERVAL);
                }
                
                // Finally set progress straight to that value.
                final int index = Math.round(nearestIndex);
                mHandler.post(new Runnable() {
                    public void run() {
                        setSelectedIndex(index);
                    }
                });
            } catch (InterruptedException ex) {
                Log.d("SLV", "Trim animation thread interrupted.");
            } finally {
                mCurrentAnimationThread = null;
            }
        }
    }
    
    /**
     * Animation runnable.
     */
    public class SwipeAnimationThread extends Thread {
        
        /**
         * Sleep interval between updates.
         */
        private final long SLEEP_INTERVAL = 20l; 
        
        /**
         * Default max time.
         */
        private final float DEFAULT_MAX_TIME = 3.0f;
        
        /**
         * Handler to post UI updates.
         */
        private Handler mHandler;
        
        /**
         * Current scrolling speed.
         */
        private float mCurrentSpeed;
        
        /**
         * Swipe direction.
         */
        private int mDirection;
        
        /**
         * Max animation time.
         */
        private float mMaxTime = DEFAULT_MAX_TIME;
        
        /**
         * Current animation time.
         */
        private float mCurrentTime = 0.0f;
        
        /**
         * Current timestamp.
         */
        private long mLastTimestamp = 0;
        
        /**
         * Main constructor.
         * 
         * @param handler Handler to post updates to.
         * @param direction Swipe direction.
         * @param speed Starting speed.
         */
        public SwipeAnimationThread(Handler handler, int direction, float speed) {
            mCurrentSpeed = speed;
            mHandler = handler;
            mDirection = direction;
        }
        
        @Override
        public void run() {
            try {
                // Get the starting time basing on starting speed.
                mCurrentTime = Math.max((mMaxTime * (ru.aint.android.tutorials.math.Math.log(1.0f - mCurrentSpeed/SCROLL_SPEED_MAX, 2.0f) + 5.0f)) / 5.0f, 0.0f);
                
                mLastTimestamp = System.currentTimeMillis();
                long timestamp = 0;
                float diff = 0.0f;
                
                // Scroll while we can.
                while ((!Thread.interrupted()) && (mCurrentSpeed > SCROLL_SPEED_STOP_THRESHOLD)) {
                    // Get the time diff since last update.
                    timestamp = System.currentTimeMillis();                    
                    diff = (float)(timestamp - mLastTimestamp) / 1000.0f;
                    mLastTimestamp = timestamp;
                    
                    // Get the new time & speed.
                    mCurrentTime += diff;
                    // Magic formula for exponential ease-out.
                    mCurrentSpeed = Math.max((float)(SCROLL_SPEED_MAX * (1.0f - Math.pow(2.0f, (5.0f * (mCurrentTime/mMaxTime - 1.0f))))), 0.0f);                    
                    
                    // Post update to the UI.
                    final float progressChange = diff*mCurrentSpeed*mDirection;
                    mHandler.post(new Runnable() {
                        public void run() {
                            updateProgress(progressChange);
                        }
                    });
                    
                    // We don't need constant updates so we let system rest for some time.
                    sleep(SLEEP_INTERVAL);
                }
                
                mHandler.post(new Runnable() {
                    public void run() {
                        trim();
                    }
                });
            } catch (InterruptedException e) {
                Log.d("SLV", "Swipe animation thread interrupted.");
                mCurrentAnimationThread = null;
            }
        }
        
    }
    
    /**
     * Sets data adapter.
     * 
     * @param adapter Data adapter.
     */
    public void setAdapter(SpinnerLayoutViewAdapter adapter) {
        mAdapter = adapter;
        mMaxIndex = mAdapter.getNumberOfItems();
        mSelectedIndex = mMaxIndex / 2;
        mCurrentIndex = mSelectedIndex;
        
        this.updateSubviews();
        this.requestLayout();
    }
    
    /**
     * Internal class for drawing content. 
     */
    private class MovingTextView extends TextView {
        
        /**
         * Current position.
         */
        private Point mPosition;
        
        /**
         * Current scale.
         */
        private float mScale;
        
        /**
         * Main constructor to create view from code.
         * 
         * @param context Application context.
         */
        public MovingTextView(Context context) {
            super(context);    
            mPosition = new Point(0, 0);
        }
        
        /**
         * Sets new position & scale for the view.
         * 
         * @param position New position.
         * @param scale New scale.
         */
        public void setPositionAndScale(Point position, float scale) {
            mPosition = position;
            mScale = scale;
            this.invalidate();
        }
        
        @SuppressLint("DrawAllocation")
        @Override
        protected void onDraw(Canvas canvas) {
            Point fixedPosition = new Point(mPosition);
            fixedPosition.x += Math.round((float)canvas.getWidth()*(1.0f - mScale) / 2.0f);
            fixedPosition.y += Math.round((float)canvas.getHeight()*(1.0f - mScale) / 2.0f);
            
            canvas.translate(fixedPosition.x, fixedPosition.y);
            canvas.scale(mScale, mScale);
            super.onDraw(canvas);
        }
    }
}
