package ru.aint.android.tutorials;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class TutorialsActivity extends ListActivity {
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorials);
        
        final ListView listView = (ListView)findViewById(android.R.id.list);
        
        ListAdapter adapter = createAdapter();
        setListAdapter(adapter);
        
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final TutorialReference item = (TutorialReference)parent.getItemAtPosition(position);
                Intent intent = item.activityIntent;
                if (intent == null) {
                    return;
                }
                
                startActivity(intent);
            }
        });
    }
    
    private class TutorialReference {
        public String name;
        public String description;
        public Intent activityIntent;
        
        public TutorialReference(String newName, String newDescription, Intent newIntent) {
            name = newName;
            description = newDescription;
            activityIntent = newIntent;
        }
    }
    
    private ListAdapter createAdapter() {
        List<TutorialReference> tutorials = new ArrayList<TutorialReference>();
        tutorials.add(new TutorialReference("Styleable Text View", "Text view with custom font from XML attribute", new Intent(this, MainActivity.class)));
        tutorials.add(new TutorialReference("Spinner View", "Various implementations of spinner view", new Intent(this, SpinnerActivity.class)));
        
        TutorialsArrayAdapter adapter = new TutorialsArrayAdapter(getApplicationContext(), R.layout.tutorials_list_row, tutorials);
        return adapter;
    }
    
    private class TutorialsArrayAdapter extends ArrayAdapter<TutorialReference> {

        private List<TutorialReference> mTutorials;
        
        private Context mContext;
        
        public TutorialsArrayAdapter(Context context, int textViewResourceId,
                List<TutorialReference> objects) {
            super(context, textViewResourceId, objects);
            mContext = context;
            mTutorials = objects;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.tutorials_list_row, parent, false);
            
            TutorialReference tutorial = mTutorials.get(position);
            
            TextView nameText = (TextView)rowView.findViewById(R.id.text_name);
            nameText.setText(tutorial.name);
            
            TextView descriptionText = (TextView)rowView.findViewById(R.id.text_description);
            descriptionText.setText(tutorial.description);
            
            return rowView;
        }
    }
}
