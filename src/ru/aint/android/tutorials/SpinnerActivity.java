package ru.aint.android.tutorials;

import java.util.Random;

import ru.aint.android.tutorials.ui.SpinnerLayoutView;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SpinnerActivity extends Activity {
    
    private SpinnerLayoutView mSpinnerView;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinners);
        
        mSpinnerView = (SpinnerLayoutView)findViewById(R.id.spinner_layout);
        mSpinnerView.setAdapter(new SpinnerLayoutView.SpinnerLayoutViewAdapter() {            
            @Override
            public int getNumberOfItems() {
                return 100;
            }
            
            @Override
            public String getItemAtIndex(int index) {
                return String.format("%02d", index);
            }
        });
        
        Button resizeButton = (Button)findViewById(R.id.resize);
        resizeButton.setOnClickListener(new View.OnClickListener() {            
            @Override
            public void onClick(View v) {
                mSpinnerView.setFontSize(mSpinnerView.getFontSize()+10);
            }
        });
        
        Button colorizeButton = (Button)findViewById(R.id.colorize);
        colorizeButton.setOnClickListener(new View.OnClickListener() {            
            @Override
            public void onClick(View v) {
                Random r = new Random();
                mSpinnerView.setFontColor(Color.argb(255, r.nextInt(256), r.nextInt(256), r.nextInt(256)));
            }
        });
    }
}
